package com.retroroots.samgsuncodingchallange.util.enums

class SharedPrefs
{
    enum class IDs
    {
        FINGER_AUTH;
    }

    enum class Keys
    {
        FAILED_LOGIN_ATTEMPT;
    }
}