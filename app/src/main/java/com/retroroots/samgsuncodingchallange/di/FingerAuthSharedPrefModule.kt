package com.retroroots.samgsuncodingchallange.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.retroroots.samgsuncodingchallange.data.sharedPref.FingerAuthSharedPrefLocalSource
import com.retroroots.samgsuncodingchallange.util.enums.SharedPrefs
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object FingerAuthSharedPrefModule
{
    @Singleton
    @Provides
    fun provideSharedPref(@ApplicationContext context: Context):SharedPreferences =
        context.getSharedPreferences(SharedPrefs.IDs.FINGER_AUTH.toString(), Context.MODE_PRIVATE)

    @Singleton
    @Provides
    fun provideFingerAuthSharedPrefLocalSource(sharedPreferences: SharedPreferences):FingerAuthSharedPrefLocalSource =
        FingerAuthSharedPrefLocalSource(sharedPreferences)

}