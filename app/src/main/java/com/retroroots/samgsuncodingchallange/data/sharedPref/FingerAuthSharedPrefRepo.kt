package com.retroroots.samgsuncodingchallange.data.sharedPref

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FingerAuthSharedPrefRepo @Inject constructor(private val localSource: FingerAuthSharedPrefLocalSource )
{
    fun GetFailedAttemptsCount() = localSource.GetFailedAttemptCount()

    fun IncrementFailedAttemptBy(totalAttempts: Int)
    {
        localSource.IncrementFailedAttemptBy(totalAttempts)
    }
}