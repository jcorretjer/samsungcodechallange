package com.retroroots.samgsuncodingchallange.data.sharedPref

import androidx.lifecycle.MutableLiveData

interface FingerAuthSharedPrefDao
{
    fun GetFailedAttemptCount(): MutableLiveData<Int>

    fun IncrementFailedAttemptBy(incrementBy: Int)
}