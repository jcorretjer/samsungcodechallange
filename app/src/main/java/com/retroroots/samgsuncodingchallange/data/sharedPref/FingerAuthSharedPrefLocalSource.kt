package com.retroroots.samgsuncodingchallange.data.sharedPref

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import com.retroroots.samgsuncodingchallange.util.enums.SharedPrefs

class FingerAuthSharedPrefLocalSource(private val sharedPref: SharedPreferences): FingerAuthSharedPrefDao
{
    override fun GetFailedAttemptCount(): MutableLiveData<Int> {
       return MutableLiveData(sharedPref.getInt(SharedPrefs.Keys.FAILED_LOGIN_ATTEMPT.toString(), 0))
    }

    override fun IncrementFailedAttemptBy(incrementBy: Int)
    {
        sharedPref.edit().putInt(SharedPrefs.Keys.FAILED_LOGIN_ATTEMPT.toString(), sharedPref.getInt(SharedPrefs.Keys.FAILED_LOGIN_ATTEMPT.toString(), 0) + incrementBy).apply()
    }
}