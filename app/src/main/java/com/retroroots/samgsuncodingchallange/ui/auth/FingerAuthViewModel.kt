package com.retroroots.samgsuncodingchallange.ui.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.retroroots.samgsuncodingchallange.data.sharedPref.FingerAuthSharedPrefRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FingerAuthViewModel @Inject constructor(private val repo: FingerAuthSharedPrefRepo): ViewModel()
{
    private var failedAttemptsCount = MutableLiveData(DEFAULT_FAILED_ATTEMPTS)

    fun GetFailedAttempts() = failedAttemptsCount

    fun IncrementFailedAttemptBy(incrementBy: Int)
    {
        failedAttemptsCount.value = failedAttemptsCount.value?.plus(incrementBy)
    }

    companion object
    {
        private const val DEFAULT_FAILED_ATTEMPTS = 0
    }
}