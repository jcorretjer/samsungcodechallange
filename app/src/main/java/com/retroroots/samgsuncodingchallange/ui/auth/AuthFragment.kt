package com.retroroots.samgsuncodingchallange.ui.auth

import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.retroroots.samgsuncodingchallange.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthFragment : Fragment(R.layout.fragment_auth) {

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        val FINGER_AUTH_VIEW_MODEL by activityViewModels<FingerAuthViewModel>()

        //Reset failed attempts
        if(FINGER_AUTH_VIEW_MODEL.GetFailedAttempts().value!! > 0)
            FINGER_AUTH_VIEW_MODEL.IncrementFailedAttemptBy(FINGER_AUTH_VIEW_MODEL.GetFailedAttempts().value!! * -1)

        view.findViewById<Button>(R.id.authBtn).setOnClickListener {
            //Validate fingerprint scanning is enabled
            if(!(requireActivity().getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager).isKeyguardSecure)
                Toast.makeText(requireContext(), resources.getString(R.string.fingerprintDisabledErrorMsg), Toast.LENGTH_LONG).show()

            else
            {
                BiometricPrompt.Builder(requireContext())
                        .setTitle(requireActivity().resources.getString(R.string.fingerPrintPromptTitleTxt))
                        .setDescription(requireActivity().resources.getString(R.string.fingerPrintPromptDescTxt))
                        .setNegativeButton(
                                requireActivity().resources.getString(R.string.fingerPrintPromptCancelBtnTxt),
                                requireActivity().mainExecutor,
                                DialogInterface.OnClickListener { _, _ ->
                            Toast.makeText(requireContext(), "Cancelled", Toast.LENGTH_LONG).show()
                        })
                        .build()
                        .authenticate(CancellationSignal(), requireActivity().mainExecutor, object : BiometricPrompt.AuthenticationCallback()
                        {
                            override fun onAuthenticationError(errorCode: Int, errString: CharSequence?)
                            {
                                super.onAuthenticationError(errorCode, errString)

                                Toast.makeText(requireContext(), "Error($errorCode): $errString", Toast.LENGTH_LONG).show()
                            }

                            override fun onAuthenticationFailed()
                            {
                                super.onAuthenticationFailed()

                                FINGER_AUTH_VIEW_MODEL.IncrementFailedAttemptBy(1)
                            }

                            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?)
                            {
                                super.onAuthenticationSucceeded(result)

                                val action = AuthFragmentDirections.actionAuthFragmentToWelcomeFragment()

                                findNavController().navigate(action)
                            }
                        })
            }
        }
    }

}