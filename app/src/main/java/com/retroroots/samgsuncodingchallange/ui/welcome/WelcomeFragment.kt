package com.retroroots.samgsuncodingchallange.ui.welcome

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.retroroots.samgsuncodingchallange.R
import com.retroroots.samgsuncodingchallange.ui.auth.FingerAuthViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WelcomeFragment : Fragment(R.layout.fragment_welcome) {
    private val FINGER_AUTH_VIEW_MODEL by activityViewModels<FingerAuthViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val failedLoginTxtVw = view.findViewById<TextView>(R.id.failedLoginTxtVw)

        FINGER_AUTH_VIEW_MODEL.GetFailedAttempts().observe(viewLifecycleOwner,
                {
                    failedLoginTxtVw.setText(resources.getString(R.string.failedLoginAttemptsTxt, it))
                })
    }
}